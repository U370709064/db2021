use company;
#UPDATE customers SET Country = REPLACE("Country,'\n',");
select count(CustomerID),Country
from customers
group by Country
order by  count(CustomerID)desc;

create view USAusaCustomerNameContactName as 
select CustomerName, ContactName
from	customers
where Country ="USA";

SELECT * FROM usa;

#set @avg_price
create or replace view products_above_avg as
select ProductName,Price
from products
where Price>(select avg(Price) from products);

SELECT * FROM products_above_avg;